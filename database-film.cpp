#include <istream>
#include <stdlib.h>
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
#include <list>
#include <algorithm>

using namespace std;

typedef struct treeNode //Deklarasi Tree Node 
{
  int year;
  string title;
  list < string > actors;
  treeNode *left;
  treeNode *right;

} *treePtr;

treePtr root = NULL;

//Prototype Fungsi
treePtr fillTree (treePtr p, int y, string t, list < string > a);
void print_titles (treePtr root);
void print_movies_year (treePtr p, int key);
void print_actors_movies (treePtr root, string name);
void print_movies_actors (treePtr root, string title);

int
main ()
{

  ifstream inFile ("daftar-film.txt");
  string x;
  treePtr root = NULL;
  int count = 0;
  if (inFile.is_open ()) //Cek apakah file sukses terbuka
    {

      while (getline (inFile, x)) //ambil judul
	{
	  if (x == "") //Cek spasi
	    {
	      continue;
	    }
	  count++;

	  int index = x.find_last_of (" "); // kode untuk menghilangkan "()" dari tahun
	  string title = x.substr (0, index);
	  string year = x.substr (index + 2, x.length () - index - 3);

	  list < string > actor; // Deklarasi Linked list 
	  int counter = 0;
	  while (getline (inFile, x)) // Looping untuk membaca aktor kedalam list
	    {
	      if (x == "")
		{
		  break;
		}
	      else
		{
		  actor.push_back (x);
		}
	    }

      root = fillTree (root, atoi (year.c_str ()), title, actor); //Mengisi Binary Search Tree
	}
    }

  int choice;
  int quit = 0;

  do				//User interface loop
    {
      cout << 
	"\n================================================= \n|\tSelamat Datang di SDA Film Store \t| \n=================================================";
      cout <<
	"\n|   (1) Ingin melihat daftar film?              |\n|   (2) Mencari judul film berdasarkan aktor?   |";
      cout << 
	"\n|   (3) Mencari berdasarkan tahun?              |\n|   (4) Mencari berdasarkan judul film?         |";
      cout << 
	"\n|   (0) Keluar dari SDA Film Store              |\n=================================================" << endl;
      cin >> choice; 

      switch (choice)
	{

	case 0:
	  system("cls");	
	  cout << "\n================================================= \n|\tTerima Kasih Sudah Berkunjung \t\t| \n=================================================" << endl;
	  quit = 1;
	  break;
	case 1: //untuk print judul film
	  system("cls");
	  cout << "\nDaftar Film yang Tersedia : \n" << endl;
	  print_titles (root);
	  cout << "\n" << endl;
	  system("pause");
	  system("cls");
	  break;  
	case 2:
	  { system("cls");
	    string name;
	    cout << "Silahkan ketik nama aktor : " << endl;
	    cin.ignore ();
	    getline (cin, name); //mengambil input nama dari user, lalu inisialisasi nama to name
	    cout << endl;
	    cout << "Film yang dibintangi oleh " << name << ":" << endl;
	    print_actors_movies (root, name); // untuk print  film berdasarkan input nama aktor
	    cout << "\n" << endl;
	    system("pause");
	    system("cls");
	    break;
	  }

	case 3:
	  system("cls");	
	  int year;
	  cout << "Silahkan ketik tahun yang ingin dicari : " << endl;
	  cin >> year;
	  cout << endl;
	  cout << "Daftar film yang rilis tahun " << year << ":" << endl;
	  print_movies_year (root, year); //untuk print film berdasarkan input tahun
	  cout << "\n" << endl;
	  system("pause");
	  system("cls");
	  break;

	case 4:
	  {
	  	system("cls");
	    string title;
	    cout << "Silahkan ketik judul film: " << endl;
	    cin.ignore ();
	    getline (cin, title);//mengambil input nama dari user, lalu inisialisasi judul to title
	    cout << endl;
	    cout << "Judul dan aktor yang bermain dalam film " << title << ":" << endl;
	    print_movies_actors (root, title);//untuk print film berdasarkan input judul film
	    cout << "\n" << endl;
	    system("pause");
	    system("cls");
	    break;
	  }

	default: //Default case
	  cout << "Nilai inputan salah, silahkan coba lagi" << endl;

	}

    }
  while (quit == 0);
  
  return 0;
}

//Fungsi Program

treePtr
fillTree (treePtr p, int y, string t, list < string > a)//isi Tree
{
  treePtr n = new treeNode;
  n->year = y;
  n->title = t;
  n->actors = a;
  n->left = NULL;
  n->right = NULL;
  if (p == NULL) //First node
    {
      p = n;
    }
  else
    {
      treePtr prev = p;
      treePtr curr = p;
      while (curr != NULL) // mengatur tree menggunakan tahun sebagai kunci
	{
	  if (curr->year > y)
	    {
	      prev = curr;
	      curr = curr->left;
	    }
	  else
	    {
	      prev = curr;
	      curr = curr->right;
	    }
	}
      if (prev->year > y)
	{
	  prev->left = n;
	}
      else
	{
	  prev->right = n;
	}
    }

  return p;
}

void
print_titles (treePtr root) //Print Judul Film
{
  if (root == NULL)
    return;
  if (root->left) // Iterasi melalui pencetakan pohon secara berurutan
    print_titles (root->left);
  cout << root->title << endl;
  if (root->right)
    print_titles (root->right);
}

void
print_movies_year (treePtr p, int key) //untuk print film berdasarkan input tahun
{

  if (p == NULL)
    return;
  if (p->left) //Iterasi melalui pencetakan pohon secara berurutan
    print_movies_year (p->left, key);
  if (p->year == key)
    {
      cout << p->title << endl;
    }
  if (p->right)
    print_movies_year (p->right, key);
}

void
print_actors_movies (treePtr root, string name)//untuk print  film berdasarkan input nama aktor
{
  if (root == NULL)
    return;
  if (root->left)
    print_actors_movies (root->left, name);
	// loop untuk iterasi melalui linked list untuk menemukan aktor
  for (list < string >::iterator it = root->actors.begin ();
       it != root->actors.end (); ++it)
    {

      if (*it == name) //print judul jika aktor ditemukan dalam list
	{
	  cout << root->title << endl;
	}
    }
  if (root->right)
    print_actors_movies (root->right, name);

}


void //untuk print film berdasarkan input judul film
print_movies_actors (treePtr root, string title)
{
  if (root == NULL)
    return;
  if (root->left)
    print_movies_actors (root->left, title);

  if (root->title == title)
    { //loop untuk print jika judul ditemukan didalam node tree
      for (list < string >::iterator it = root->actors.begin (); 
			it != root->actors.end (); ++it)	
{
	  cout << *it << endl;

	}
    }

  if (root->right)
    print_movies_actors (root->right, title);
}
